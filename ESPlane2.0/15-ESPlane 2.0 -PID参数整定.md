﻿@[toc]
## 一、PID参数整定流程

crazyfile `Rate PID`调整过程
1. 先调整`Rate `模式，将`rollType `,`pitchType` 和 `yawType`都调整为`RATE`
2. 将 `ATTITUDE`模式对应的 `roll`, `pitch` 和 `yaw`的`KP`,`KI`和`KD`调整为`0.0`，仅保留`Rate `相关的参数
3. 将`RATE`模式对应的 `roll`, `pitch` 和 `yaw` 的`KI`和`KD`调整为`0.0`，先调整比例控制`KP`
4. 烧写代码，使用cfclient的param功能开始在线进行`KP`的调整
****
5. 注意，使用cfclient修改后的参数，掉电是不保存的。
6. 注意安全，因为在PID调整期间会出现超调的情况
7. 先固定住飞行器，让其只能进行`pitch`轴的翻转。逐渐增加`pitch`对应的`KP`,直到飞机出现前后的震荡（超调） 
8. 当出现严重的震荡时，可以稍微降低`KP`（ Once you reach the point of instability, tune it down 5-10 points），然后即可确定`KP`参数
9. 同样的方法调整 `roll` 
10. 最后同样的方法调整`yaw` 
11. 下面调整 `KI`，该参数用于消除稳态误差，因为如果不引入该参数，只有比例调整的话，飞机受到重力等干扰会在0位置上线摆动。设置 `KI`的初始值为`KP`的50%。
12.  当`KI`增大到一定程度，也会导致飞机不稳定的晃动，但是`KI`造成的晃动频率会相比`KP`带来的震动，频率更小。然后以造成这个状态的 `KI`为基础确定 `KI`的值（This is your critical KI, and so tune down 5-10 points.）
13. 同样的方法调整 `roll` 和 `yaw`
14.  yaw axis, except KI is usually around 80%+ of KP.

****
以上完成了`Rate `模式参数的调整
****
下面开始调试 `Attitude PID`
 
14. 确保`Rate PID`调整已经完成。
15. 将`rollType `,`pitchType` 和 `yawType`都调整为`ANGLE`，意味着飞机已经进入attitude mode。
16. 改变 `roll`和`pitch`的`KI`和`KD`为`0.0`，将`Yaw` 的 `KP``KI``KD`都设置为`0.0` 。
17. 烧写代码，使用cfclient的param功能开始在线进行`KP`的调整。
18. 将`roll`和`pitch`的`KP `设置为`3.5`，寻找任何不稳定性，例如振荡。持续增加KP，直到达到极限。
19. 如果您发现`KP`导致不稳定，如果此时已经高于`4`，需要将`RATE`模式的 `KP`和`KI`稍微降低5-10点。这使您在调整姿势模式时更加“自由”
20. 要调整KI，请再次缓慢增加KI。不稳定性的状态是产生低频振荡。


## 二、 PID调试问题记录191025

### 1. 逆时针慢速的转向抖动
 
起飞1秒后，飞机出现逆时针转动和机身摇晃（多次测试均是逆时针慢速的转向），但能看到在pitch和roll方向有修正作用，但是存在上下的颠簸。
**先说下最终解决方法**：发现最主要原因为飞行模式选错，姿态按照X型进行计算，但是电机输出确实按照+型计算，两者冲突，将电机输出也改成X型，解决问题！！！
***
以下是当时解决问题的一些猜想和实验过程，可以忽略不看！！

* **猜测一：yaw轴修正方向不对？**

**测试1：** 使用自制工具，固定住pitch和raw两个方向，只给yaw轴自由度，在飞机运行状态下，手动旋转yaw轴（引入干扰）。
**结果1：** 发现飞机能够慢速修正到初始方向，因此该猜想1不成立。
**TODO1:** 需要进一步查实，使飞机修正的贡献最大的参数（kp? ki?）

 **测试2：** 直接去掉yaw轴修正（对应kp 、ki、kd均设为为0） 
 **结果2：** 飞机yaw轴失去控制，转动速度更快，说明yaw修正大方向还是对的

> 其他测试：使用遥控控制yaw轴，转向正常，方向正确。

![在这里插入图片描述](https://img-blog.csdnimg.cn/20191025200826898.jpg?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzIwNTE1NDYx,size_16,color_FFFFFF,t_70)

**TODO2:** 奇怪的问题记录：
在现象1出现同时，记录yaw轴上kp， ki，kd分别对应的输出分量。第一个波谷出现时，飞机在慢速逆时针转动，kp输出值应该为正数，为什么为负？之后出现两次波峰，然后突然被拉回。传感器问题？**需要进一步查实，传感器输出是否异常**

![在这里插入图片描述](https://img-blog.csdnimg.cn/20191025191522521.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzIwNTE1NDYx,size_16,color_FFFFFF,t_70)



* **2. 猜测二：其他轴的波动导致的转动？**

**测试1：** 起飞观察pitch轴各个计算分量的输出
**结果1：** 能看到在上下震荡，存在超调可能。

![在这里插入图片描述](https://img-blog.csdnimg.cn/20191025203015257.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzIwNTE1NDYx,size_16,color_FFFFFF,t_70)

**测试2：** 用绳子将飞机固定住yaw和roll，只保留pitch轴自由度，上下波动（引入干扰），观测飞机修正情况
**结果2：** 发现飞机能正常稳定的修正，未见明显的超调。

![在这里插入图片描述](https://img-blog.csdnimg.cn/20191025205538677.jpg?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzIwNTE1NDYx,size_16,color_FFFFFF,t_70)

***

### 2. 电机输出值上下波动过大

电机输出值上下波动过大，在前后两个控制周期里，同一个电机输出可能出现最小值到最大值的波动。
TODO: 需要检查源码，是否有控制分量占比过大，导致最终拟合给电机输出波动太大
**191025修正**：输出给mos管的pwm频率从1.4kHz提高到300kHz，crazyfile原工程为328Khz。ESPlane1.0 arduino代码pwm频率为1.4khz。[修正原因](https://forum.bitcraze.io/viewtopic.php?f=5&t=1405&p=7559&hilit=328#p7559)
**191030再次修正**：将电机输出改为了30kHz（8bit分辨率），在300kHz会出现PWM输出异常




