﻿@[toc]
## Crazyfile工程预览
ESPlane2.0将以Crazyfile工程为移植对象，该工程具有较为科学和完善的代码架构，在稳定性和可扩展性上具有很大的优势，源代码使用GPL3.0开源协议，需要在修改后保证开源，可以用于商业用途。ESPlane将保持Crazyfile姿态计算，控制算法等关键代码，结合ESP32和ESP_IDF特点，使其能够在单颗ESP32芯片上运行，并具有WiFi控制等功能。本章将对Crazyfile工程做一次整体的功能介绍：

### 1. 系统架构

#### 1.1 硬件架构

crazyfile 2.0 使用了两颗主芯片，一颗STM32F4，一颗nRF51822。前者处理飞控大多数任务，后者负责无线通信和电源管理。两颗芯片通过串口通信，官方定制了双芯片的协作协议。

![Crazyflie 2.X System Architecture](https://img-blog.csdnimg.cn/20190808160002590.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzIwNTE1NDYx,size_16,color_FFFFFF,t_70)

#### 1.2 算法信息流

* 简单来说，飞控软件的工作原理如下图所示：传感器测量->角度拟合->自稳定控制->命令响应->计算电机PWM。


![在这里插入图片描述](https://img-blog.csdnimg.cn/20190808160507202.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzIwNTE1NDYx,size_16,color_FFFFFF,t_70)

* 飞行控制软件中，一般有多个负反馈控制回路，分别用来控制飞行姿态，空间位置。这些控制回路以不同的频率进行控制和纠偏。


![在这里插入图片描述](https://img-blog.csdnimg.cn/20190808160518761.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzIwNTE1NDYx,size_16,color_FFFFFF,t_70)

### 2. 传感器

[mpu9250 9轴传感器](https://blog.csdn.net/suxiang198/article/details/75529921)是MPU6050的升级版，除了集成陀螺仪和加速度计，还集成了三轴磁罗盘传感器。crazyfile 2.0版本开始支持该传感器。当然MPU6050自身也有I2C子接口，可以进行透传，连接一个独立磁罗盘也能同样起到简化接口。

### 3. 启动顺序

When the Crazyflie 2.X is powered on it will automatically go through a short sequence of events to get ready for flight.

1. 开机自检：检测 Crazyflie 2.X 硬件是否正常。
2. 传感器校准： Crazyflie 2.X 读取传感器值作为基准值，校准时必须放置水平，并且在一段时间内保持静止。
3. Ready to fly!

### 4. Crazyfile通信协议CRTP

crazyfile并未使用车船航模（如PIX系列）通用的MAVLINK协议，而是自己定义了一套通信协议，用于传输飞行数据、调整参数等。[CRTP 协议](https://wiki.bitcraze.io/doc:crazyflie:crtp:index) (Crazy RealTime Protocol)。

[部分中文介绍](http://www.crazepony.com/book/wiki/comm-protocol.html)

![CRTP协议](https://img-blog.csdnimg.cn/20190809143906903.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzIwNTE1NDYx,size_16,color_FFFFFF,t_70)

### 5. cflib: CRTP协议python支持包

**cflib是CRTP协议的支持包，提供了通信协议的应用层接口，可以用于构建上位机。** [github: crazyflie-lib-python](https://github.com/bitcraze/crazyflie-lib-python)

>cflib is an API written in Python that is used to communicate with the Crazyflie and Crazyflie 2.0 quadcopters. It is intended to be used by client software to communicate with and control a Crazyflie quadcopter. For instance the cfclient Crazyflie PC client uses the cflib.

### 6. Crazyflie PC client 

[github: crazyflie-clients-python](https://github.com/bitcraze/crazyflie-clients-python)
**crazyfile官方基于cflib开发的上位机，可以用于刷机、通信和调试**

>The Crazyflie PC client enables flashing and controlling the Crazyflie. It implements the user interface and high-level control (for example gamepad handling). The communication with Crazyflie and the implementation of the CRTP protocol to control the Crazflie is handled by the cflib project.


![在这里插入图片描述](https://img-blog.csdnimg.cn/20190808172917516.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzIwNTE1NDYx,size_16,color_FFFFFF,t_70)

> To launch the GUI application in the source folder type: python3 bin/cfclient


### 7. Crazyfile 2.0 I2C改动
连续且高频访问传感器会对主控带来压力。Crazyfile 2.0 I2C优化方案-
Instead of polling it uses interrupts for I2C writing and DMA for I2C reading to keep MCU intervention low.

[官网说明：Improving the Crazyflie 2.0 I2C driver](https://www.bitcraze.io/2016/09/improving-the-crazyflie-2-0-i2c-driver/)


### 8. 视觉引导方案

crazyfile原生支持航迹规划等功能，可以用于高级开发。
https://wiki.bitcraze.io/doc:crazyflie:vision:index

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190808162148965.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzIwNTE1NDYx,size_16,color_FFFFFF,t_70)



### 9. Crazyflie指示灯定义

![crazyfile](https://imgconvert.csdnimg.cn/aHR0cHM6Ly93d3cuYml0Y3JhemUuaW8vaW1hZ2VzL2dldHRpbmctc3RhcnRlZC9mcm9udENGLnBuZw?x-oss-process=image/format,png)

* 通电并且自检通过： 蓝色 LED (2 and 3) 完全被点亮，右前方 LED (1) 每秒闪烁两次红色。
* 通电自检通过但是传感器未校准： 蓝色 LED (2 and 3) 被点亮，右前方 LED (1) 每2秒闪烁一次，此时务必把Crazyflie 2.X 放在水平面，并且在一段时间内保持静止。
* 无线已连接： 左前方 LED (4)红色和绿色交替闪烁。
* 低电量： 右前方 LED (1) 红色常亮。此时应该降落并充电。
* 充电中：左后方 LED (3) 闪烁，同事右后方蓝色 LED (4) 常亮。
* Boot loader 模式： 后方蓝色 LED (2 and 3) 大约每秒闪烁一次。
* 自检失败： 右前方 LED (1) 以5次快闪1次慢闪交替闪烁。

### 10. crazyfile螺旋桨编号与方向

![](https://imgconvert.csdnimg.cn/aHR0cHM6Ly93d3cuYml0Y3JhemUuaW8vaW1hZ2VzL2dldHRpbmctc3RhcnRlZC9jZjJfcHJvcHMucG5n?x-oss-process=image/format,png)



### 11. 关于crazyfile开源协议

* crazyfile由瑞士三个工程师设计开发。该项目现在由深圳Seeed Studio公司负责生产和销售。
* GPL3.0：GNU General Public License v3.0 可以作为商业用途，但是软件再次分发，则需要遵循 GPL 开源。



