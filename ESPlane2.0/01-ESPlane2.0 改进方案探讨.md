﻿@[toc]
## 一、ESPlane FC V1.0 Issues
**#1. 无操作下水平飘飞**
**#2. 无操作下轴向旋转**
**#3. 机头方向难以区分**
****
## 二、Solutions
**#1. 无操作下水平飘飞**
**可能有以下原因：**

1. 加速度计没有有效的校准。优化传感器校准过程，上位机增加校准选项。
2. 算法拟合出的角度没有及时的跟随实际的偏角。优化调试方法，绘制曲线。
3. 陀螺仪的温漂问题，导致积分误差。
4. 增加空间定位传感器，**如：光流传感器、摄像头、GPS（室外）、其他**
****
**#2. 无操作下轴向旋转**
1. 优化算法，使用IMU z轴测量值锁定转向
2. 增加方向传感器，**如：磁罗盘传感器**

**#3. 机头方向难以区分**
1. 调整螺旋桨颜色，前黑后红
2. 螺旋桨附近增加航行灯，前绿后红
***
## 三、 ESPlane2.0功能探讨
### 1. 飞行功能：
|功能|传感器支持|接口|备注|
|:--:|:--:|:--:|:--:|
|多自由度控制：上下旋转前后左右|+mpu6050|I2C|选配磁罗盘|
|自稳模式|+mpu6050|I2C|选配磁罗盘|
|一键起飞|+mpu6050|I2C|选配磁罗盘|
|一键降落|+mpu6050|I2C|选配磁罗盘|
|滚转空翻|+mpu6050|I2C|选配磁罗盘|
|定高模式|+气压计MS5611/ 激光VL53L0x|I2C|超声波尺寸过大|
|定点模式|+迷你光流PMW3901/ 底部摄像头|SPI|GPS尺寸大且只能室外|
|无头模式|+磁罗盘HMC5883|I2C| |

### 2. 探索飞行功能：
|功能|传感器支持|接口|备注|
|:--:|:--:|:--:|:--:|
|语音控制|定点模式(底部摄像头)+ESP-ADF| | |
|多机编队飞行|定点模式(底部摄像头)+地面标志+ESP-MESH| | |

### 3. 硬件资源汇总：

|模组名称|接口|尺寸|价格|链接|
|:--:|:--:|:--:|:--:|:--:|
|PMW3901迷你光流模组|串口(自带SPI转串口)|14.2*11.04mm|32-45元|[链接](https://item.taobao.com/item.htm?spm=a230r.1.14.26.715425c6BBYmmh&id=566169525970&ns=1&abbucket=4#detail)|
|VL53L0X/VL53L1X 激光 ToF模组|I2C|25*10.7mm|34-59元|[链接](https://detail.tmall.com/item.htm?spm=a230r.1.14.16.3762461fpm7EJ9&id=589406747288&cm_id=140105335569ed55e27b&abbucket=4)|
|PMW3901+VL53L0x二合一模组|I2C@400K+SPI@2M|27.5*16.5mm|98-128元|[链接](https://detail.tmall.com/item.htm?id=585754830280&ali_refid=a3_430582_1006:1109983619:N:UWNFJK2wBzuZTKP+qmmu1Q==:4ffc3b4588a1039dc49235c5de2d7376&ali_trackid=1_4ffc3b4588a1039dc49235c5de2d7376&spm=a230r.1.14.3&skuId=4140061383850)|
|气压计MS5611-01BA03模组|I2C/SPI|19*13mm|54-60元|[链接](https://detail.tmall.com/item.htm?id=41281679152&ali_refid=a3_430583_1006:1109983619:N:7yXz5RCSLStVeHMEs9gjdg==:15f10f662f307c73a924b7bb352cc3d6&ali_trackid=1_15f10f662f307c73a924b7bb352cc3d6&spm=a230r.1.14.3)|
|磁罗盘HMC5883模组|I2C|13.9*18.5mm|29-30元|[链接](https://detail.tmall.com/item.htm?spm=a230r.1.14.23.27dc9729Iw3QNF&id=549257230087&ns=1&abbucket=4)|


|芯片名称|接口|封装|价格|链接|
|:--:|:--:|:--:|:--:|:--:|
|PMW3901迷你光流芯片|SPI@2M|28PIN-COB|31-33元|[链接](https://detail.1688.com/offer/569079828937.html)|
|VL53L1X 激光 ToF芯片|I2C|LGA-12|25-29元|[链接](https://item.taobao.com/item.htm?spm=a230r.1.14.172.af755af0o429m8&id=595169911927&ns=1&abbucket=4#detail)|
|磁罗盘HMC5883芯片|I2C|LCC-16|24-26元|[链接](https://detail.tmall.com/item.htm?spm=a230r.1.14.98.27dc9729Vjp0tW&id=550390458060&ns=1&abbucket=4)|
|气压计MS5611-01BA03芯片|I2C/SPI|SMD-8|20-25元|[链接](https://detail.tmall.com/item.htm?spm=a230r.1.14.58.7e784a1bVduo0T&id=520066088704&ns=1&abbucket=4)|

### 4. 参考链接：
1. [PMW3901光流模块 定点 激光测距 Minifly四轴 2M/4M可选模块](https://detail.tmall.com/item.htm?id=585754830280&ali_refid=a3_430582_1006:1109983619:N:UWNFJK2wBzuZTKP+qmmu1Q==:4ffc3b4588a1039dc49235c5de2d7376&ali_trackid=1_4ffc3b4588a1039dc49235c5de2d7376&spm=a230r.1.14.3&skuId=4140061383850)
2. [迷你光流传感器 定点悬停 室内正常灯光瓷砖地板上也可使用](https://item.taobao.com/item.htm?spm=a230r.1.14.26.715425c6BBYmmh&id=566169525970&ns=1&abbucket=4#detail)
3. [正点原子 PMW3901光流模块 定点 激光测距 Minifly四轴 小尺寸](https://detail.tmall.com/item.htm?spm=a230r.1.14.13.4f97295cw9bQQ1&id=582161506671&ns=1&abbucket=4)
4. [VL53L0X V2激光测距传感器模块 ToF飞行时间测距 配套光学盖片](https://detail.tmall.com/item.htm?spm=a230r.1.14.16.3762461fpm7EJ9&id=589406747288&cm_id=140105335569ed55e27b&abbucket=4)
5. [Risym HMC5883L模块 电子指南针罗盘实验板 三轴磁场传感器模块](https://detail.tmall.com/item.htm?spm=a230r.1.14.23.27dc9729Iw3QNF&id=549257230087&ns=1&abbucket=4)
6. [GY-63 MS5611-01BA03 高精度 高度传感器模块 气压传感器模块](https://detail.tmall.com/item.htm?id=41281679152&ali_refid=a3_430583_1006:1109983619:N:7yXz5RCSLStVeHMEs9gjdg==:15f10f662f307c73a924b7bb352cc3d6&ali_trackid=1_15f10f662f307c73a924b7bb352cc3d6&spm=a230r.1.14.3)
7. [VL53L1X 传感器芯片VL53L1CXV0FY/1封装LGA-12 ST意法激光测距4米](https://item.taobao.com/item.htm?spm=a230r.1.14.172.af755af0o429m8&id=595169911927&ns=1&abbucket=4#detail)
8. [云辉 MS5611-01BA03 561101BA03气压/压力传感器贴片SMD-8](https://detail.tmall.com/item.htm?spm=a230r.1.14.58.7e784a1bVduo0T&id=520066088704&ns=1&abbucket=4)
9. [朗果 HMC5883L HMC5883 L883 传感器 贴片LCC-16](https://detail.tmall.com/item.htm?spm=a230r.1.14.98.27dc9729Vjp0tW&id=550390458060&ns=1&abbucket=4)
10. [PMW3901-MB PIXART台湾原相CJMCU-3901 XY平移 光流传感器pmw3901](https://detail.1688.com/offer/569079828937.html) 
