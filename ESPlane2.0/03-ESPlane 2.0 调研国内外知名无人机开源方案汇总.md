﻿@[toc]
## 一、 小四轴开源方案
### 1.1. 汇总表
|开源方案名|功能|主控|OS|外设配置|开源协议
|:--:|:--:|:--:|:--:|:--:|:--:|
|[Crazyfile](https://github.com/bitcraze/crazyflie-firmware)|功能|STM32F1|FreeRTOS|九轴*+气压+选配mini光流|GPL3|
|[Cleanflight](https://github.com/cleanflight)|多种飞行模式|STM32F1/F3/F4/F7|裸机|九轴*|GPL3|
|[Betaflight](https://github.com/betaflight/betaflight)|功能|STM32F3/F4/F7|裸机|九轴*+gps|GPL3|
|[Crazepony](http://www.crazepony.com/index.html)|基础飞行|STM32F1|裸机或RT-Thread|九轴*|GPL3|
> 九轴*：3轴陀螺仪+3轴加速度计+3轴磁罗盘

***

### 1.2 Crazefile


见Crazefile工程预览

### 1.3 Crazepony

来自中国开发者，灵感来自与cleanflight，crazyfile。

参考资料：
1. [Crazepony官方wiki](http://www.crazepony.com/wiki/crazepony1-intro.html)
2. [Crazepony开源四轴飞行器-开发笔记](https://www.bookstack.cn/read/crazepony/wiki-hardware-base.md)
3. [Crazepony裸机版本代码仓库](https://github.com/Crazepony/crazepony-firmware-none)
4. [Crazepony RT-Thread版本代码仓库](https://github.com/Crazepony/crazepony-firmware-rtthread)
5. [release版本代码](http://www.crazepony.com/download.html)

## 二、 大四轴开源方案

|开源方案名|功能|主控|OS|外设配置|开源协议|
|:--:|:--:|:--:|:--:|:--:|:--:|
|APM|功能|AVR|OS|外设配置|开源协议|
|PIX|功能|STM32|OS|外设配置|开源协议|
|开源方案名|功能|主控|OS|外设配置|开源协议|

***
