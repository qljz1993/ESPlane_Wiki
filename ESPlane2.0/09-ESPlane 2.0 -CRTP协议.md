﻿
@[toc]
## 一、CRTP协议简介

ESPlane项目继承crazyfile项目使用的CRTP协议，用于传输飞行数据、调整参数等。[CRTP 协议](https://wiki.bitcraze.io/doc:crazyflie:crtp:index) (Crazy RealTime Protocol)。[部分中文介绍](http://www.crazepony.com/book/wiki/comm-protocol.html)

> 链接过程：CRTP is designed to be state-less, so there's no handshaking procedure that is needed. Any command can be sent at any time, but for some logging/param/mem commands the TOC (table of contents) needs to be downloaded in order for the host to be able to send the correct information. The implementation of the Pyton API will download the param/log/mem TOC at connect in order to be able to use all the functionality.

## 二、CRTP协议架构图

[Communication stack](https://wiki.bitcraze.io/doc:crazyflie:dev:arch:communication)

![CRTP协议](https://img-blog.csdnimg.cn/20190809143906903.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzIwNTE1NDYx,size_16,color_FFFFFF,t_70)

![Communication stack](https://img-blog.csdnimg.cn/20191007144238648.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzIwNTE1NDYx,size_16,color_FFFFFF,t_70)


## 三、CRTP协议层级

![CRTP](https://img-blog.csdnimg.cn/20190921222043537.jpg)

* 第一层：CRTP Link。负责在PC与Crazyflie之间传输数据包。主要处理包长度，包错误信息。`The link is responsible for transferring the packet to/from the Crazyflie. Currently there's two types of physical links implemented: A radio link using the Crazyradio and a wired link using USB directly to the Crazyflie (only implemented for 2.0).`
*  第二层：CRTP Packet handling。将数据包传递给相应的飞行器子系统和PC控制软件。`The protocol used for sending data to/from the Crazyflie is named CRTP (Crazyflie RealTime Protocol). In it's current state the protocol isn't real-time, but there's bits over in the header to implement this functionality.`
*  第三层：Application/ports。代表发送和接受数据包的相应子系统。`Each module is assigned a port, which works much like the ports in UDP. Sending data on a specific port will make sure it's routed to the registered module for this port. The data that is sent to/from a specific port also contains a channel id of 3 bits. This channel id can be used to more easily route the data once inside the module.`
> 在协议层不设置校验码，因此需要在链路层添加校验。Since no CRC/checksum is added to the CRTP packet it's the link layers responsibility to check the integrity of the packets.

**Implemented link drivers:**

| Link ||
|--|--|	
| UART	| 可以接wifi或蓝牙透传 Uart link, mainly used in early development |
| USB |	To be implemented USB link. (Crazyflie USB port) |
| radio |	2.4 GHz NordicSemi Radio link (any nRF24L01 compatible radio, eg. Crazyradio dongle) |

>注意**UART 接口：crazyfile2 已弃用**
The serial port is configured in 115200 8N1. CRTP packets are sent and received asynchronously using the following packet format:


**Current port allocation:**

| Port	 | Target	| Used for |
|:--:|:--:|:--:|
| 0	| Console	| Read console text that is printed to the console on the Crazyflie using consoleprintf | 
| 2	| Parameters	| Get/set parameters from the Crazyflie. Parameters are defined using a macro in the Crazyflie source-code | 
| 3	| Commander	| Sending control set-points for the roll/pitch/yaw/thrust regulators | 
|4	|Memory access	|Accessing non-volatile memories like 1-wire and I2C (only supported for Crazyflie 2.0)|
| 5	| Log	| Set up log blocks with variables that will be sent back to the Crazyflie at a specified period. Log variables are defined using a macro in the Crazyflie source-code | 
| 6	| Localization	|Packets related to localization|
|7	 | Generic Setpoint	| Allows to send setpoint and control modes|
| 13 | Platform	| Used for misc platform control, like debugging and power off|
| 14	| Client-side debugging	| Debugging the UI and exists only in the Crazyflie Python API and not in the Crazyflie itself. |  
|15	| Link layer	| Used to control and query the communication link| 

> `In the firmware most modules (that are connected to ports) are implemented as tasks. Each task is blocking on a message delivery queue where incoming CRTP packets are delivered. At start up each of these tasks (and other modules) register a callback with the communication layer for their pre-defined ports.`

* [ 新comm_protocol 英文参考](https://www.bitcraze.io/docs/crazyflie-firmware/master/ctrp_index/)
* [ 老comm_protocol 英文参考](http://wiki.bitcraze.se/projects:crazyflie:firmware:comm_protocol)
* [关于LOG和PARAM宏定义使用： 8. crazyfile Table of content (TOC)](https://blog.csdn.net/qq_20515461/article/details/98957829)或英文原文[Logging and parameter frameworks](https://www.bitcraze.io/docs/crazyflie-firmware/master/logparam/)


**CRTP Packet structure:**
> Each CRTP packet is 32 bytes, of which 1 byte is taken by the header. This gives a total payload of 31 bytes per packet. The header holds the port (8 bits), channel (2 bits) and reserved (2 bits).

```
  7   6   5   4   3   2   1   0
+---+---+---+---+---+---+---+---+
|     Port      |  Res. | Chan. | 
+---+---+---+---+---+---+---+---+
|            DATA 0             |
+---+---+---+---+---+---+---+---+
:   :   :   :   :   :   :   :   :
+---+---+---+---+---+---+---+---+
|            DATA 30            |
+---+---+---+---+---+---+---+---+
```

| Field	| Byte	| Bit	| Description| 
|:--:|:--:|:--:|:--:| 
| Header	| 0	| 0-1	| The destination channel | 
| | 0	| 2-3| 	Reserved for transport layer| 
| | 0	| 4-7	| The destination port| 
| Data	| 1-31	| 	The data in the packet| 



## 四、CRTP命令格式

### 1. Commander

[Commander：](https://wiki.bitcraze.io/projects:crazyflie:crtp:commander#commander)
The commander port is used to send control set-points for the roll/pitch/yaw/thrust regulators in the Crazyflie. As soon as the communication link has been established these packets can be sent and the values are valid until the next packet is received.
这个port用来发送命令，发送的命令保持到下一个命令到来

```
        +-------+-------+-------+-------+
        | ROLL  | PITCH |  YAW  |THRUST |
        +-------+-------+-------+-------+
Length      4       4       4       2      bytes
```

| Name	| Byte | Size | Type | Comment|
|:--:|:--:|:--:|:--:|:--:|
| ROLL	| 0-3	| 4	| float	| The pitch set-point | 
| PITCH	| 4-7	| 4	| float	| The roll set-point | 
| YAW	| 8-11	| 4	| float	| The yaw set-point | 
| THRUST	| 12-13	| 2	| uint16_t	| The thrust set-point | 

### 2. Console

 [Console:](https://wiki.bitcraze.io/projects:crazyflie:firmware:console#console)
This port is used as a one-way text console for text printed on the Crazyflie using the consoleprintf function.

```
Answer (Copter to PC):
        +---------//-----------+
        | PRINTED CONSOLE TEXT |
        +---------//-----------+
Length          0-30
```

The contents of the buffer on the copter side is sent if any of the following is fulfilled:
* 缓冲区满 The output buffer (of 31 bytes) is full
* 接收到换行符 A “newline” character has to be send (\n and/or \r)
* 发送刷新命令 A flush command as been issued


### 3. Logging通信建立过程

[Logging通信建立过程](https://wiki.bitcraze.io/projects:crazyflie:firmware:comm_protocol#logging)


### 4. 其他port

[port allocation:](https://wiki.bitcraze.io/projects:crazyflie:firmware:comm_protocol#overview)

示例（crazyfile2 无法使用）：

 **发送目标点 send setpoints to the commander send:**

```
0xaa 0xaa 0x30 0x0e 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x3e
```

## 五、cflib: CRTP协议python支持包

* **cflib是CRTP协议的支持包，提供了通信协议的应用层接口，可以用于构建上位机。** [github: crazyflie-lib-python](https://github.com/bitcraze/crazyflie-lib-python)
*  对于crazyfile 固件中每一个使用ＣＲＴＰ协议的组件，在ｃｆｌｉｂ中都有一个脚本与其对应。`For every module that is used to send/receive CRTP packets there's a Python API object that is used to interact with it. For instance for logging there's a module named log.c in the Crazyflie firmware and a class named Log in the Python API. This part of the API is then used to directly interact with the logging functionality in the Crazyflie.`
* 详情请查阅下一章节

>cflib is an API written in Python that is used to communicate with the Crazyflie and Crazyflie 2.0 quadcopters. It is intended to be used by client software to communicate with and control a Crazyflie quadcopter. For instance the cfclient Crazyflie PC client uses the cflib.


## 六、crazyfile工程涉及到的其他通信协议

### 1. Syslink

[Syslink](https://wiki.bitcraze.io/doc:crazyflie:syslink:index)是对ＣＲＴＰ协议在数据链路层的又一层包装，用于ｎｒｆ51通信芯片与ＳＴＭ３２主控之间（ＵＡＲＴ）数据传输。
* 物理格式

```
/* Frame format:
 * +----+-----+------+-----+=============+-----+-----+
 * |  START   | TYPE | LEN | DATA        |   CKSUM   |
 * +----+-----+------+-----+=============+-----+-----+
 *
 *START is 2 bytes constant, 0xBC 0xCF.
 *TYPE defines the type of packet
 *LENGTH and type are uint8_t, defines the data length.
 *DATA 数据包，这里可以是ＣＲＴＰ包，或其他数据包
 *CKSUM is 2 bytes Fletcher 8 bit checksum. calculated with TYPE, LEN and DATA.[See rfc1146](https://tools.ietf.org/html/rfc1146)
 */
```

>* 数据以1MBaud（1,000,000,）传输。In Crazyflie 2.0 syslink is transmitted on serial port at 1MBaud. It is a packet-based protocol.
>* 数据为小数端存放。Unless otherwise specified, all numbers are encoded in low-endian form.


* Packet group

|Group	|Name	|Description|
|--|--|--|
|0x00	|RADIO	|Radio related packets. For data and configuration|
|0x10	|PM	|Power management|
|0x20	|OW	|One wire memory access|

> Group只显示了ＴＹＰＥ的高４位，第四位要根据具体的指令类型确定。A packet type has its group in the high nibble and the type in the low nibble. In the rest of the page packet type are written with group.


* Packet tpye

|TYPE|Name||
|--|--|--|
|0x00|RADIO_RAW|This packet carries the raw radio packet.|
|0x01|RADIO_CHANNEL|Packet sent to the NRF51 to set the radio channel to use.|
|0x02|RADIO_DATARATE|Packet sent to the NRF51 to set the radio datarate to use|
| 0x11|SYSLINK_PM_ONOFF_SWITCHOFF|he NRF51 switch OFF the system and goes in deep sleep|

>以上只列出一部分，更多详情：https://wiki.bitcraze.io/doc:crazyflie:syslink:index

### 2. USB  protocol to Crazyradio dongle

[USB and Radio protocol of the Crazyradio dongle](https://www.bitcraze.io/docs/crazyradio-firmware/master/usb_radio_protocol/)
该协议用于上位机和crazyradio硬件之间的ＵＳＢ通信，可用于设置射频的模式、功率、地址等

### 3. onewire

onewire(单总线)是DALLAS公司推出的外围串行扩展总线，传输时钟信号又传输数据，而且能够进行双向通信，具有节省I/O口线、资源结构简单、成本低廉、便于总线扩展和维护等诸多优点。常用到单总线的器件，一般是温度传感器，如DS18B20、DS2431。
crazyfile工程使用onewire协议，实现NRF51与扩展模块的通信，读取扩展模块身份信息等。

