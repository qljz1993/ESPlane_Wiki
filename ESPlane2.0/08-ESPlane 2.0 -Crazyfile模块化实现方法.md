﻿@[toc]

## 1. 单总线接口one wire 

onewire(单总线)是DALLAS公司推出的外围串行扩展总线，传输时钟信号又传输数据，而且能够进行双向通信，具有节省I/O口线、资源结构简单、成本低廉、便于总线扩展和维护等诸多优点。常用到单总线的器件，一般是温度传感器，如DS18B20、DS2431。

## 2. crazyfile工程挂载自定义驱动方法

**Deck drivers（crazyfile开发方向，未完成）**

The Deck API allows to easily communicates with decks installed on the Crazyflie. The deck API is still in development, it will:
-   Enumerates installed Deck and initialize drivers (*pretty much
    done*)
-   Provides easy to use arduino-like API to access (*started*)
-   Optionally provides a task to run in (arduino-like setup and loop)
    (*not started yet*)
Decks are enumerated automatically using a One Wire (OW) memory soldered on the deck PCB. The Deck driver API is using a declarative syntax to register deck drivers and initialize them when the proper deck is installed.

**Minimal driver 启动流程**
-   At startup the decks are enumerated and if a deck has the name
    \"meMyled\", it will be initialized.
-   Init is called on all initialized driver and then test is called.
-   The init and test functions are both optional (we have some board
    with only init and event some with only test). If absent just remove
    the *.init* or *.test* initialisation.
-   In this mode no task are created so to run some code at regular
    intervale the code needs to deal with freeRtos or with the other
    parts of the Crazyflie code.

**Minimal driver 示例**
Deck drivers are in the deck/driver/src folder.

1.Create this file named hello.c in the deck/driver/src folder:

```{.c}
#define DEBUG_MODULE "HelloDeck"
#include "debug.h"

#include "deck.h"


static void helloInit()
{
  DEBUG_PRINT("Hello Crazyflie 2.0 deck world!\n");
}

static bool helloTest()
{
  DEBUG_PRINT("Hello test passed!\n");
  return true;
}

static const DeckDriver helloDriver = {
  .name = "myHello",
  .init = helloInit,
  .test = helloTest,
};

DECK_DRIVER(helloDriver);
```
2.Add this to the Makefile, after the line # Decks:

```{.make}
PROJ_OBJ += hello.o
```
3.Enabling the driver
Decks can have a memory that contains its name. In our case the hello driver would be initialised only when a deck identified as \"myHello\" is installed on the Crazyflie. For development purpose it is possible to force enabling a deck driver with a compile flag. To do so create the file tools/make/config.mk with the content:

```{.make}
CFLAGS += -DDECK_FORCE=myHello

DEBUG=1
```

> DEBUG=1 allows to get more information from the Crazyflie console when it starts. Debug should not be enabled if you intend to fly the Crazyflie out of the lab (it disables the watchdog).

4.Compile, flash and run!
The output will be similar to the following:

```{.bash}
crazyflie-firmware$ make
(...)
  CC    hello.o
(...)
Crazyflie 2.0 build!
Build 44:61f3d878233d (2015.08.1-44) MODIFIED
Version extracted from git
Crazyloader build!
   text    data     bss     dec     hex filename
 130660    1636   29828  162124   2794c cf2.elf
rm version.c
crazyflie-firmware$ make cload
../crazyflie-clients-python/bin/cfloader flash cf2.bin stm32-fw
Restart the Crazyflie you want to bootload in the next
 10 seconds ...
 done!
Connected to bootloader on Crazyflie 2.0 (version=0x10)
Target info: nrf51 (0xFE)
Flash pages: 232 | Page size: 1024 | Buffer pages: 1 | Start page: 88
144 KBytes of flash available for firmware image.
Target info: stm32 (0xFF)
Flash pages: 1024 | Page size: 1024 | Buffer pages: 10 | Start page: 16
1008 KBytes of flash available for firmware image.

Flashing 1 of 1 to stm32 (fw): 161867 bytes (159 pages) ..........10..........10..........10..........10..........10..........10..........10..........10..........10..........10..........10..........10..........10..........10..........10.........9
Reset in firmware mode ...
$ 
```
Now you can connect your Crazyflie with the client and see your driver
in the console!

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190912171743686.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzIwNTE1NDYx,size_16,color_FFFFFF,t_70)


