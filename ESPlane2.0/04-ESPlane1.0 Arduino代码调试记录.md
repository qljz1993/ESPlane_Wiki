﻿## 问题记录：

 1. 每次WiFi连接时，飞机进行传感器校准，如果此时未放置到平整地面，飞机无法正常飞行。


![在这里插入图片描述](https://img-blog.csdnimg.cn/20190801194431414.png)
* 上图显示飞机在连接WiFi时未放置于平整地面，导致放置到平整地面后，角度不正常，飞机始终处于纠偏状态。

1. 偶尔出现飞机角度不能快速更新


![在这里插入图片描述](https://img-blog.csdnimg.cn/20190801194714673.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzIwNTE1NDYx,size_16,color_FFFFFF,t_70)

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190801194726547.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzIwNTE1NDYx,size_16,color_FFFFFF,t_70)

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190801194737968.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzIwNTE1NDYx,size_16,color_FFFFFF,t_70)

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190801194757853.png)

* 俯仰角从-12度角修正到-0度，花费50秒。飞机已经失控。
