﻿@[toc]
***
## 一、调试方法

### 1. 万向节调试

[视频链接](https://v.youku.com/v_show/id_XNjM1MjMyMTIw.html?spm=a2h0j.11185381.listitem_page1.5!10~A)
视频展示了一种调试方法可以在后期调试时借鉴，能够极大提高调试效率

![万向节调试](https://img-blog.csdnimg.cn/20190808110432182.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzIwNTE1NDYx,size_16,color_FFFFFF,t_70)

## 二、其他飞行器知识

### 1.飞行器地效

当飞行器离地过低，会出现溜冰的效果，水平方向难以控制，如果使用了气压定高，甚至导致高度摆动。飞到足够高度即可

### 2. 螺旋桨平衡校准

如果想得到更好的飞行效果，可以进行螺旋桨的筛选或处理。使用平衡校准后的螺旋桨可以减少机身震动，减少对传感器的影响，同时能极大的降低噪音。

![Balancing propellers](https://img-blog.csdnimg.cn/20190808141303161.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzIwNTE1NDYx,size_16,color_FFFFFF,t_70)






