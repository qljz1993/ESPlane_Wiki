﻿@[toc]
***

## 一、官方示例

使用ESP-IDF构建工程，需要遵循一定的规则，否则可能出现编写错误。乐鑫开源了大量的示例工程或者解决方案，可以直接复制修改：

* [快速上手：esp-jumpstart](https://github.com/espressif/esp-jumpstart)
* [esp-idf：附带example](https://github.com/espressif/esp-idf/tree/master/examples)
* [空模板工程：esp-idf-template](https://github.com/espressif/esp-idf-template)
* [iot解决方案：esp-iot-solution](https://github.com/espressif/esp-iot-solution)

***

## 二、项目目录结构

![示例项目目录树](https://img-blog.csdnimg.cn/20190724163034348.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzIwNTE1NDYx,size_16,color_FFFFFF,t_70)

* Makefile应该放在项目根目录，该文件所在目录会被编译系统自动保存为==PROJECT_NAME==
* sdkconfig由make menuconfig自动生成，同级目录还会自动生成sdkconfig.old、sdkconfig.defaults，用于恢复上一次设置或默认设置
* components目录下可以不设置，构建系统会从IDF_PATH目录调用component源代码，如果将要使用到的component复制到项目目录。构建系统会使用当前目录下component，不影响IDF_PATH内源代码。
* component1,component2为组件目录，里面包含组件源代码（官方封装库），其中==Kconfig==用于声明menuconfig的选项，通过界面交互修改组件的宏定义。==component.mk==用于配置组件的各种行为，具体可以参考：[预设的组件变量](https://docs.espressif.com/projects/esp-idf/zh_CN/latest/api-guides/build-system-legacy.html#preset-component-variables)

* main目录放置工程主要源文件，该目录被官方定义为“伪组件”，和组件目录遵循相同的编译规则。



## 三、学习笔记

### 1. 默认构建行为

**main目录和component目录都需要包含component.mk文件，哪怕是空的**。如果文件为空，则组件的默认构建行为会被设置为：
* 当前目录的所有*.c *.cpp *.cc文件将参与编译
* 当前目录子目录“include”下的所有头文件，会被添加到全局搜索路径

[官方解读](https://docs.espressif.com/projects/esp-idf/zh_CN/latest/api-guides/build-system-legacy.html#minimal-component-makefile)

### 2. 组件默认搜索顺序

与其他编译器不同，这里将使用最后搜索到的路径，如果工程内存在同名component，将覆盖ESP-IDF的源代码。

```
components默认搜索顺序：
$(IDF_PATH)/components
$(PROJECT_PATH)/components
$(PROJECT_PATH)/main
EXTRA_COMPONENT_DIRS
```


