﻿@[toc]
## ESP32芯片命名规则
![产品型号和订购信息](https://img-blog.csdnimg.cn/20190928130208902.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzIwNTE1NDYx,size_16,color_FFFFFF,t_70)

## ESP32芯片型号一览
![订购信息](https://img-blog.csdnimg.cn/20190928130356879.png)
## ESP32官方模组使用芯片一览
|=================== |============  |===========  |========= |==== | ===============|
|:--:|:--:|:--:|:--:|:--:|:--:|
|Module              |Chip         |Flash, MB    |PSRAM, MB |Ant.  |Dimensions, mm |
|=================== |==========  |===========  |========= | ==== |===============|
|ESP32-WROOM-32     |  ESP32-D0WDQ6 | 4          |  --       |  MIFA  |18 × 25.5 × 3.1|
|ESP32-WROOM-32D   |   ESP32-D0WD   | 4, 8, or 16 | --     |    MIFA  |18 × 25.5 × 3.1|
|ESP32-WROOM-32U     | ESP32-D0WD   | 4, 8, or 16 | --    |     U.FL  |18 × 19.2 × 3.1|
|ESP32-SOLO-1       |  ESP32-S0WD  |  4          | --      |   MIFA  |18 × 25.5 × 3.1|
|ESP32-WROVER (PCB) |  ESP32-D0WDQ6 | 4      |      8    |      MIFA  |18 × 31.4 × 3.3|
|ESP32-WROVER (IPEX)|  ESP32-D0WDQ6 | 4      |      8    |      U.FL  |18 × 31.4 × 3.3|
|ESP32-WROVER-B    |   ESP32-D0WD  |  4, 8, or 16 | 8     |     MIFA | 18 × 31.4 × 3.3|
|ESP32-WROVER-IB     | ESP32-D0WD   | 4, 8, or 16 | 8      |    U.FL | 18 × 31.4 × 3.3|
|===================  |============ | ===========  |========= | ====|===============|

* MIFA - 蛇形倒 F 天线；
* U.FL - U.FL/IPEX 天线连接器；

