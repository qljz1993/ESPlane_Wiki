﻿@[toc]

## 一、 汇总表格

 **这里仅收集起飞重量小于250克的微型无人机信息**
> 根据民航局政策，自2017年6月1日起，正式对最大起飞重量为250克以上的无人机实施实名制注册。


|公司|型号|卖点|上市时间|售价|
|:--:|:--:|:--:|:--:|:--:|:--:|
|法国Parrot|[Mambo Fly](http://www.parrot.com.cn/edu/app/) |接入多种教育平台|2018？|￥498起|
|中国大疆&深圳睿炽科|[TELLO EDU](https://www.ryzerobotics.com/cn/tello-edu?site=brandsite&from=landing_page)|益智编程/多机编队|2018.12|￥999.00|
|中国小米|[米兔遥控小飞机](https://www.mi.com/minidrone/)|游戏对战|2018.05|￥399.00|
|淘宝网小厂家|迷你无人机|儿童玩具|陆续推出|￥80-200不等|
|美国AxisDrones|[Vidius](https://www.pcmag.com/review/343768/axis-drones-vidius)|超微型航拍|2017.01 |$95.00 已下架|
|中国零度|[DOBBY](https://www.zerotech.com/uav-uas/dobby/)|便携式空拍机|2016.12|￥2399 已下架|

## 二、 STEAM教育领域: 

### 2.1. 大疆TELLO EDU

[大疆TELLO EDU](https://www.ryzerobotics.com/cn/tello-edu?site=brandsite&from=landing_page)

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190731141504917.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzIwNTE1NDYx,size_16,color_FFFFFF,t_70)

* SWIFT PLAYGROUND：Apple公司ipad app，互动有趣的方式来学习 Swift。“Tello 星际旅行”教程*将编程以太空探险形式展现，你可以在 iPad 屏幕上根据提示编写代码、学习 Swift 语言
*  支持Scratch、Python 和 Swift 等编程语言
* AI应用编写，开发视频流，提供[物体识别DEMO](https://github.com/dji-sdk)

### 2.2. Parrot MAMBO 系列

[ Parrot MAMBO 系列](http://www.parrot.com.cn/edu/app/)

>STEAM代表科学（Science），技术（Technology），工程（Engineering），艺术（Arts），数学（Mathematics）。STEAM教育就是集科学，技术，工程，艺术，数学多学科融合的综合教育。

**K12教育无人机平台：MAMBO 系列
高等院校教育使用的无人机: Bebop 系列**

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190731105932742.jpg?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzIwNTE1NDYx,size_16,color_FFFFFF,t_70)

**目前已经接入以下平台：**
* SWIFT PLAYGROUND：Apple公司ipad app，互动有趣的方式来学习 Swift
* TYNKER：全球知名的创意编程学习平台
* BLOCKLY：由WORKBENCH平台提供技术支持，美国知名在线学习中心。
* PYTHON：通过让孩子们对无人机进行编程并飞行操控，从而强化孩子（K-12）对STEM概念（编程，数学，和更多）的认知理解
* JAVASCRIPT：MINIDRONE-JS是一款极易于使用的无人机脚本语言。该脚本语言支持PARROT无人机使用的双向通信协议, 支持接收传感器更新并根据XML规范发送命令。      
* MATHWORKS SIMULINK：可对PARROT MINIDRONES的飞行控制算法进行自主设计和构建

* DEVELOPERS：该SDK主要通过C语言编写，它可为UNIX，ANDROID和IOS系统提供数据库。可帮助你对无人机进行连接、操控、接收视频流、保存和下载媒体文件（照片和视频）、发送和播放自动驾驶飞行计划、更新等操作。
***

## 三、 便携式空拍机

### 3.1. 零度Dobby娱乐空拍

[零度Dobby娱乐空拍](https://www.zerotech.com/uav-uas/dobby/)

* 口袋无人机定位

***

## 四、 益智玩具 

### 4.1. 小米米兔小飞机

[小米米兔小飞机](https://www.mi.com/minidrone/)

![米兔小飞机](https://img-blog.csdnimg.cn/20190801110741283.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzIwNTE1NDYx,size_16,color_FFFFFF,t_70)

* 红外对战模式：多台米兔无人机空中对战，使用红外传感器打击对方前后特地区域，有“减血”效果，血量耗尽无法飞行。
* 5.8G WiFi图传，720P摄像头，操控距离50m。
* 

### 4.2. Axis VIDIUS ™

[Axis VIDIUS ™世界最小FPV无人机](https://www.pcmag.com/review/343768/axis-drones-vidius)

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190731122636576.jpg?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzIwNTE1NDYx,size_16,color_FFFFFF,t_70)

### 4.3. 淘宝网小厂商

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190731201526146.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzIwNTE1NDYx,size_16,color_FFFFFF,t_70)

>销量前三名，配置、方案、售价基本一致，能够实现基本的飞行功能。
1. [无人机高清专业小型小学生儿童男孩玩具航拍四轴飞行器遥控飞机](https://detail.tmall.com/item.htm?spm=a230r.1.14.5.7d7b73dcW0iqHm&id=592317986510&ns=1&abbucket=4&skuId=4074645224429)
月销量1.5w+ |价格根据高低配置115-436元|



