﻿@[toc]
## 一、工程目录结构

### 1. 总目录结构

ESPlane2.0基于esp_idf `release/v3.3` 和crazyfile`master:commit 13aa1372bc3ba5b14922fc2f9d8999270882021a` (https://github.com/qljz1993/crazyflie-firmware.git)。按照IDF编写风格，以组件的形式组织整个代码工程，其中main中包含app_main函数，用于启动整个工程。components文件夹包含工程中修改的一些esp_idf组件(未修改的组件默认从IDF_PATH查找)，各个传感器驱动，硬件抽象层（hal），飞控程序模块（modules）等：

![在这里插入图片描述](https://img-blog.csdnimg.cn/20191028161441788.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzIwNTE1NDYx,size_16,color_FFFFFF,t_70)

### 2. 驱动层

### 3. 硬件抽象层

硬件抽象层作为传感器驱动层与飞控程序层中间的桥梁，是该工程能够实现移植的关键，硬件抽象层包含以下文件：

![hal](https://img-blog.csdnimg.cn/20191028161529794.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzIwNTE1NDYx,size_16,color_FFFFFF,t_70)

### 4. 飞控程序层

飞控程序层是该工程的核心代码层，用于姿态计算，控制量计算，协议通信等，该层包含以下文件：

![modules](https://img-blog.csdnimg.cn/20191028161502980.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzIwNTE1NDYx,size_16,color_FFFFFF,t_70)

### 5. 扩展层

参见[08ESPlane 2.0 无人机开发笔记-Crazyfile模块化实现方法](https://blog.csdn.net/qq_20515461/article/details/102820385)，crazyfile工程挂载自定义驱动方法
-Deck drivers（esplane未完成该部分移植20191028）

## 二、代码启动过程

### 1. 系统启动过程

![在这里插入图片描述](https://img-blog.csdnimg.cn/20191028155018141.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzIwNTE1NDYx,size_16,color_FFFFFF,t_70)

### 2. 关键TASK-自稳模式

![自稳模式](https://img-blog.csdnimg.cn/20191028195017574.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzIwNTE1NDYx,size_16,color_FFFFFF,t_70)

### 3. 关键数据结构

![在这里插入图片描述](https://img-blog.csdnimg.cn/20191028152923889.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzIwNTE1NDYx,size_16,color_FFFFFF,t_70)




