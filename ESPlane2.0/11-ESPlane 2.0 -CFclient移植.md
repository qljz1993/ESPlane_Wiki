﻿@[toc]
## 一、CFclient预览

CFclient 是基于 [cflib](https://blog.csdn.net/qq_20515461/article/details/102179552) 软件包开发的上位机程序，ESPlane项目将对该上位机进行裁剪和调整，满足功能设计需求。

![cfclient控制台界面](https://img-blog.csdnimg.cn/20191022104951393.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzIwNTE1NDYx,size_16,color_FFFFFF,t_70)

![cflib图形绘制界面](https://img-blog.csdnimg.cn/20191022105041379.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzIwNTE1NDYx,size_16,color_FFFFFF,t_70)

## 二、CFclient架构

![Architecture](https://img-blog.csdnimg.cn/20191022115149326.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzIwNTE1NDYx,size_16,color_FFFFFF,t_70)

项目中有很多相关的文件，例如配置文件和缓存文件，其中JSON文件用来存储配置信息。关于配置信息中内容的解读：点击[User configuration file](https://www.bitcraze.io/docs/crazyflie-clients-python/master/dev_info_client/)
