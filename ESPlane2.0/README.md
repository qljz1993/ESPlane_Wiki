﻿
# ESPlane2.0

**Drones powered by ESP32&ESP_IDF&Crazyfile**

***

## 一、简介

ESPlane2.0将以Crazyfile工程为移植对象，该工程具有较为科学和完善的代码架构，在稳定性和可扩展性上具有很大的优势，源代码使用GPL3.0开源协议，在修改后保证开源可以用于商业用途。ESPlane将保持Crazyfile姿态计算，控制算法等关键代码，结合ESP32和ESP_IDF特点，使其能够在单颗ESP32芯片上运行，并具有WiFi控制，自动组网等功能。

![ESPlane](https://img-blog.csdnimg.cn/20191030202043361.jpg?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzIwNTE1NDYx,size_16,color_FFFFFF,t_70)


![A swarm of drones exploring the environment, avoiding obstacles and each other. (Guus Schoonewille, TU Delft)](https://img-blog.csdnimg.cn/20191030202634944.jpg?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzIwNTE1NDYx,size_16,color_FFFFFF,t_70)

## 二、已实现功能
1. 自稳定模式
2. 对接cfclient上位机
3. espilot app控制
4. 手柄控制


## 三、配置表

### 传感器

1. I2C ： I2C_NUM_0 
	* MPU6050
	* HMC5883L
	* MS5611



### ESPlane引脚配置

|引脚|功能|备注|
|:--:|:--:|:--:|
|GPIO21|SDA|I2C0 data|
|GPIO22|SCL|I2C0 clock|
|GPIO14 |SRV_2 | MPU6050 interrupt|
|GPIO16 | RX2 |
|GPIO17 | TX2 |
|GPIO27 |SRV_3 | UART2 CTS|
|GPIO26 |SRV_4 | UART2 RTS %need modify|
|GPIO23|LED_RED|LED_1|
|GPIO5|LED_GREEN|LED_2|
|GPIO18|LED_BLUE|LED_3|
|GPIO4|MOT_1|
|GPIO33|MOT_2|
|GPIO32|MOT_3|
|GPIO25|MOT_4|
|TXD0|
|RXD0|
|GPIO35|ADC_7_BAT|VBAT/2|

### 实验版引脚配置 CORE BOARD V2

|引脚|功能|备注|
|:--:|:--:|:--:|
|SDA|I2C0 data|
|SCL|I2C0 clock|
|GPIO22|INT|MPU6050 interrupt|
|GPIO23|DRDY|HMC5883L data ready|
|GPIO25|LED_RED|
|GPIO26|LED_GREEN|
|GPIO27|LED_BLUE|

## 四、感谢/THANKS

1. 感谢Bitcraze开源组织提供很棒的[crazyfile](https://www.bitcraze.io/ )无人机项目代码

2. 感谢Espressif提供ESP32和[ESP_IDF操作系统](https://docs.espressif.com/projects/esp-idf/en/latest/index.html)

3. 感谢WhyEngineer提供的stm32 dsp移植库[esp_dsp](https://github.com/whyengineer/esp32-lin/tree/master/components/dsp_lib)




